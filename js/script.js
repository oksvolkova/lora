window.onload = function(){ 
	var drop_down = document.getElementsByClassName('shop-drop-down')[0];

	var link = document.getElementsByClassName('drop-down-but')[0];



	link.onclick = function(){
		drop_down.style.display = 'block';
	};

	window.onclick = function(event){
		if(event.target == drop_down){
			drop_down.style.display = "none";
		}
	};
};
$(document).ready(function(){

	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 50) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 300);
			return false;
		});
	});


});
$(document).ready(function() {
    // Show or hide the sticky footer button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('#back-to-top').fadeIn(500);
        } else {
            $('#back-to-top').fadeOut(300);
        }
    });

    // Animate the scroll to top
    $('#back-to-top').click(function(event) {
        event.preventDefault();

        $('html, body').animate({scrollTop: 0}, 300);
    })
});


window.onload = function(){ 

	var modal = document.getElementById('myModal');

	var btn = document.getElementById('myBtn');

	var span = document.getElementsByClassName('close')[0];


	btn.onclick = function(){
		modal.style.display = 'block';
	}
	span.onclick = function(){
		modal.style.display = 'none';
	}
	window.onclick = function(event){
		if(event.target == modal){
			modal.style.display = "none";
		}
	}
};



$(document).ready(function(){
		var input = $('input');
		var elem = $('.size');//div parent

		
		elem.click(function (){
			// console.log($(this).children());
			if($(this).children('input').attr('checked')){
				$(this).children('input').attr('checked', false);
			}else{
				$(this).children('input').attr('checked', 'true');
			}
		});
});
$(document).ready(function(){
		$(".drop-down-but").click(function(){
			$('.shop-drop-down').slideDown( "slow" );
		});
});

$(document).ready(function () {
    $(".drop-down-but").hover(function () {
            $('.shop-drop-down').slideDown( "slow" );
	}); 
    $('.shop-drop-down').mouseleave(function(){
            	$('.shop-drop-down').slideUp( "slow" );
            });
});


$(document).ready(function () {
	mainSrc = $('.img-hold img').attr('src');

	$('.drop-menu a').hover(function () {
		this.src = $(this).data('src');

		$('.img-hold img').attr('src', this.src);
	});
	$('.drop-menu a').mouseleave(function(){
		$('.img-hold img').attr('src', mainSrc);
	})

});

$(document).ready(function(){
	wraPpic = $('.small-img img').attr('src');
	// console.log(wraPpic);
	$('.small-img div').click(function (){
		
		this.src = $(this).data('src');

		$('.photo img').attr('src', this.src);
	});
	$('img-wrap div').mouseleave(function(){
		$('img-wrap div').attr('src',wraPpic );
	})
});

$(document).ready(function(){
	$('.photo img').click(function(){
		$('.full-size-pop-up').show("slow", function(){
			// $('.full-size-pic img');

			var fullSrc = $('.photo img').attr('src');
			console.log(fullSrc);

			$('.full-size-pic .first-pup-pic').attr('src', fullSrc);
			
		});
	});
	$('.full-size-pop-up .close').click(function(){
		$('.full-size-pop-up').hide();
	})
});

 

$(document).ready(function(){

	var width = 720;
	var animationSpeed = 1500;
	var pause = 3000;
	var currentSlide = 1;

	var $slider = $('#slider');
	var $slideContainer = $slider.find('.slides');
	var $slides = $slideContainer.find('.slide');

	var interval;

	function startSlider(){
		interval = setInterval(function(){
			$slideContainer.animate({'margin-left': "-="+width},animationSpeed, function(){
				currentSlide++;
				if(currentSlide === $slides.length){
					currentSlide = 1;
					$slideContainer.css('margin-left', 0);
				}
			});
		}, pause);
	}

	function stopSlider(){
		clearInterval(interval);
	}

	$slider.on('mouseenter', stopSlider).on('mouseleave',startSlider);

	startSlider();

});

$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide"
  });
});